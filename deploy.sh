echo "Restarting adb server"
adb kill-server
adb start-server
adb devices | tail -n +2 | cut -sf 1 | xargs -iX adb -s X install -r platforms/android/ant-build/Turtle-debug.apk
echo "APKs installed, starting application"
adb devices | tail -n +2 | cut -sf 1 | xargs -iX adb -s X shell am start -W -a android.intent.action.MAIN -n com.coolappz.Turtle/.Turtle
echo "APKs started. Remember to publish the apk by running bash publish.sh"

