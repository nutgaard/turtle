function Program(commands) {
    var blockManager = CodeblockManager.get();
    //Keep program in textual form here
    var program = commands;

    this.toText = function() {
        var s = '';
        function tab(tabs) {
            var t = '';
            for (var i = 0; i < tabs; i++)
                t += '    ';
            return t;
        }
        var indent = 0;
        return program.slice(0).map(function(element) {
            if (CodeblockDefinitions.isClosingBracket(element))
                indent--;
            var o = tab(indent) + element;
            if (CodeblockDefinitions.isOpeningBracket(element))
                indent++;
            return o;
        }).join('\n');
        return program.slice(0).join('\n');
    }
    this.toGraphics = function() {
        if (program.length > 0 && program[0]==='//#javascript'){
            return [];
        }
        return program.slice(0).map(function(command) {
            return blockManager.getBlockByCommand(command);
        });
    }
    var isValidProgram = function(graphics) {
        var allDefined = graphics.indexOf(undefined) === -1;
        console.debug('defined: ',allDefined, graphics.indexOf(undefined));
        if (!allDefined)return false;
        var noBigNumbers = true;
        graphics.forEach(function(e){
            if (CodeblockDefinitions.isActuator(e) && parseInt(e.getOptions().data[0]) > 2249){
                noBigNumbers = false;
            }
        });
        return noBigNumbers;
    }(this.toGraphics())//Verify correctness

    this.isValid = function() {
        return isValidProgram;
    }
    this.toRunnableProgram = function() {
        return new RunnableProgram(this);
    }
    this.getRaw = function() {
        return program.slice(0);
    }
}
Program.fromGraphics = function(graphicsContainer) {
    var instructions = graphicsContainer.find('li:not(.start)');
    var commands = [];
    instructions.each(function(index, element) {
        var block = Codeblock.fromHTML($(element));
        commands.push(block.getCode());
    });
    return new Program(commands);
}
Program.fromText = function(textContainer) {
    var commands = textContainer.val().split('\n')
            .filter(function(command) {
                return command.trim().length > 0;
            })
            .map(function(command) {
                return command.trim();
            });
    return new Program(commands);
}