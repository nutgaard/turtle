function CodeblockDefinitions() {
    var blocks = [];
    var extendedMode = Options.get().getOptions().extendedMode;
    
    CodeblockDefinitions.block = {};
    CodeblockDefinitions.blocks = [];
    function noopHelper() {
        return function(robot, o) {
            return robot;
        }
    }
    function noopTimerHelper() {
        return function(o) {
            return o.noopTime || 500;
        }
    }
    function moveHelper(forward) {
        var direction = forward ? 1 : -1;

        return function(robot, o) {
            var length = parseInt(o.data[0]);
            if (length === 0) {
                return robot;
            }

            var alpha = Math.toRadians(robot.angle);
            var dLeft = length * Math.sin(alpha) * direction;
            var dTop = length * Math.cos(alpha) * -direction;

            robot.left += dLeft;
            robot.top += dTop;

            return robot;
        }
    }
    function moveTimerHelper() {
        return function(o) {
            var length = parseInt(o.data[0]);
            return Math.abs(length / o.moveSpeed * 1000);
        }
    }
    function turnHelper(left) {
        var direction = left ? 1 : -1;
        return function(robot, o) {
            var dAngle = parseInt(o.data[0]);
            if (dAngle === 0) {
                return robot;
            }
            robot.angle -= dAngle * direction;

            return robot;
        }
    }
    function turnTimerHelper() {
        return function(o) {
            var length = parseInt(o.data[0]);
            return Math.abs(length / o.turnSpeed * 1000);

        }
    }

    blocks.push({
        string: 'FWD',
        color: '#0f0',
        needsInput: [{
                type: 'number',
                default: 0
            }],
        move: moveHelper(true),
        time: moveTimerHelper()
    });
    blocks.push({
        string: 'BACK',
        color: '#00f',
        needsInput: [{
                type: 'number',
                default: 0
            }],
        move: moveHelper(false),
        time: moveTimerHelper()
    });
    blocks.push({
        string: 'TL',
        color: '#ff0',
        needsInput: [{
                type: 'number',
                default: 0
            }],
        move: turnHelper(true),
        time: turnTimerHelper()
    });
    blocks.push({
        string: 'TR',
        color: '#f0f',
        needsInput: [{
                type: 'number',
                default: 0
            }],
        move: turnHelper(false),
        time: turnTimerHelper()
    });
    if (extendedMode) {
        blocks.push({
            string: 'SET',
            color: '#a0a',
            needsInput: [
                {
                    type: 'text',
                    default: 'v'
                },
                {
                    type: 'number',
                    default: 0
                }
            ],
            move: noopHelper(),
            time: noopTimerHelper()
        });
        blocks.push({
            string: 'PROC',
            color: '#0ff',
            needsInput: [{
                    type: 'text',
                    default: 'name'
                }],
            move: noopHelper(),
            time: noopTimerHelper()
        });
        blocks.push({
            string: 'END',
            color: '#f00',
            needsInput: [],
            move: noopHelper(),
            time: noopTimerHelper()
        });
        blocks.push({
            string: 'CALL',
            color: '#0aa',
            needsInput: [{
                    type: 'text',
                    default: 'name'
                }],
            move: noopHelper(),
            time: noopTimerHelper()
        });
        blocks.push({
            string: 'REP',
            color: '#aa0',
            needsInput: [{
                    type: 'number',
                    default: 1
                }],
            move: noopHelper(),
            time: noopTimerHelper()
        });
    }
    CodeblockDefinitions.block = {};
    blocks.forEach(function(block, index) {
        block.id = index + 1;
        var cb = new Codeblock(block);
        blocks[index] = cb;
        CodeblockDefinitions.block[cb.getOptions().opcode] = cb;
    });
    CodeblockDefinitions.blocks = blocks;
}
CodeblockDefinitions.isOpeningBracket = function(block) {
    if (!Options.get().getOptions().extendedMode)return false;
    if (typeof block === 'object') {
        if (block.getOptions().opcode === CodeblockDefinitions.block['PROC'].getOptions().opcode) {
            return true;
        }
        if (block.getOptions().opcode === CodeblockDefinitions.block['REP'].getOptions().opcode) {
            return true;
        }
    } else if (typeof block === 'string') {
        return CodeblockDefinitions.isOpeningBracket(CodeblockDefinitions.block[block.split(' ')[0]]);
    }
    return false;
}
CodeblockDefinitions.isClosingBracket = function(block) {
    if (!Options.get().getOptions().extendedMode)return false;
    if (typeof block === 'object') {
        if (block.getOptions().opcode === CodeblockDefinitions.block['END'].getOptions().opcode) {
            return true;
        }
    } else if (typeof block === 'string') {
        return CodeblockDefinitions.isClosingBracket(CodeblockDefinitions.block[block.split(' ')[0]]);
    }
    return false;
}
CodeblockDefinitions.isVariableSetter = function(block) {
    if (!Options.get().getOptions().extendedMode)return false;
    if (typeof block === 'object') {
        if (block.getOptions().opcode === CodeblockDefinitions.block['SET'].getOptions().opcode) {
            return true;
        }
    }
    return false;
}
CodeblockDefinitions.isActuator = function(block) {
    if (typeof block === 'object') {
        if (block.getOptions().opcode === CodeblockDefinitions.block['FWD'].getOptions().opcode) {
            return true;
        }
        if (block.getOptions().opcode === CodeblockDefinitions.block['BACK'].getOptions().opcode) {
            return true;
        }
        if (block.getOptions().opcode === CodeblockDefinitions.block['TL'].getOptions().opcode) {
            return true;
        }
        if (block.getOptions().opcode === CodeblockDefinitions.block['TR'].getOptions().opcode) {
            return true;
        }
    } else if (typeof block === 'string') {
        return CodeblockDefinitions.isActuator(CodeblockDefinitions.block[block.split(' ')[0]]);
    }
    return false;
}
CodeblockDefinitions.exists = function(opcode) {
    for (var i = 0; i < CodeblockDefinitions.blocks.length; i++) {
        if (opcode === CodeblockDefinitions.blocks[i].getOptions().opcode)
            return opcode;
    }
    return '';
}

new CodeblockDefinitions();