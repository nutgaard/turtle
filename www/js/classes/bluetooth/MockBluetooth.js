function MockBluetooth() {
    this.connection = false;
    this.hasConnection = false;
}
MockBluetooth.prototype.getDevices = function() {
    var d = new $.Deferred();
    setTimeout(function() {
        d.resolve([
            {
                class: 276,
                id: "10:BF:48:CB:00:00",
                address: "10:BF:48:CB:00:00",
                name: "FireFly-854B"
            },
            {
                class: 7936,
                id: "00:06:66:4D:00:00",
                address: "00:06:66:4D:00:00",
                name: "FireFly-854C"
            }
        ]);
    }, 500);
    return d.promise();
}
MockBluetooth.prototype.isEnabled = function() {
    var d = new $.Deferred();
    setTimeout(function() {
        d.resolve(true);
    }, 500);
    return d.promise();
}
MockBluetooth.prototype.connect = function(name) {
    console.debug('connecting')
    var d = new $.Deferred();
    this.getDevices().done(function(devices) {
        var found = false;
        for (var i = 0; i < devices.length; i++) {
            if (devices[i].name === name) {
                found = true;
                var rnd = Math.random() > 0.8;
                console.debug('this', this, this.connection);
                if (rnd) {
                    setTimeout(function() {
                        console.debug('connection rejected', this.connection);
                        d.reject('Found ID, could not connect: ' + name);
                        this.connection = false;
                        console.debug('connection rejected', this.connection);
                    }.bind(this), 500);
                } else {
                    setTimeout(function() {
                        console.debug('connection OK', this.connection);
                        d.resolve(name);
                        this.connection = true;
                        console.debug('connection OK', this.connection);
                    }.bind(this), 500);
                }
            }
        }
        if (!found) {
            d.reject("No such device");
        }
    }.bind(this));
    return d.promise();
}
MockBluetooth.prototype.disconnect = function() {
    var d = new $.Deferred();
    this.isConnected().done(function(status) {
        if (!status) {
            this.connection = status;
            d.resolve(true);
        } else {
            var rnd = Math.random() > 0.7;
            setTimeout(function() {
                if (rnd) {
                    d.reject('Fail');
                } else {
                    d.resolve('OK');
                    this.connection = false;
                }
            }.bind(this), 500);
        }
    }.bind(this));
    return d.promise();
}
MockBluetooth.prototype.isConnected = function() {
    var d = new $.Deferred();
    setTimeout(function() {
        d.resolve(this.connection)
    }.bind(this), 500);
    return d.promise();
}
MockBluetooth.prototype.send = function(codeblock) {
    var d = new $.Deferred();
    var trans = this.translate(codeblock);
    setTimeout(function() {
        console.debug('Block sent: ', trans);
        if (Math.random() > 0.9) {
            d.reject('FAIL');
        } else {
            d.resolve("OK");
        }
    }.bind(this), 500);
    return d.promise();
}
MockBluetooth.prototype.isMock = function() {
    return true;
}
MockBluetooth.prototype.translate = function(codeblock) {
    return Bluetooth.prototype.translate(codeblock);
}
