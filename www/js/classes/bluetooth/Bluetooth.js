function Bluetooth() {
}
Bluetooth.prototype.getDevices = function() {
    var d = new $.Deferred();
    bluetoothSerial.list(function(devices) {
        d.resolve(devices);
    }, function(error) {
        var str = 'bluetoothSerial::list failed';
        console.error(str, error);
        d.reject(str + " " + error);
    });
    return d.promise();
}
Bluetooth.prototype.isEnabled = function() {
    var d = new $.Deferred();
    bluetoothSerial.isEnabled(function() {
        d.resolve(true);
    }, function() {
        d.resolve(false);
    });
    return d.promise();
}
Bluetooth.prototype.connect = function(name) {
    var d = new $.Deferred();
    this.getDevices().done(function(devices) {
        for (var i = 0; i < devices.length; i++) {
            if (devices[i].name === name) {
                bluetoothSerial.connect(devices[i].address, function() {
                    bluetoothSerial.subscribe('\n', function(data) {
                        console.log('Response',data);
                    }, function(){});
                    d.resolve(name);
                }, function(error) {
                    d.reject('Found ID, could not connect: ' + error);
                    console.debug('Found ID, could not connect: ', error);
                });
                break;
            }
        }
    });
    return d.promise();
}
Bluetooth.prototype.disconnect = function() {
    var d = new $.Deferred();
    this.isConnected().done(function(status) {
        if (!status) {
            d.resolve(true);
        } else {
            bluetoothSerial.disconnect(function() {
                d.resolve(true);
            }, function(error) {
                d.reject(error);
            });
        }
    });
    return d.promise();
}
Bluetooth.prototype.isConnected = function() {
    var d = new $.Deferred();
    bluetoothSerial.isConnected(function() {
        d.resolve(true);
    }, function() {
        d.resolve(false);
    });
    return d.promise();
}
Bluetooth.prototype.send = function(codeblock) {
    var translated = this.translate(codeblock);
    console.debug('sending string:', translated);
    var d = $.Deferred();
    bluetoothSerial.write(translated, function() {
        console.debug('String has been sent:::::',translated)
        d.resolve();
    }, function(error) {
        d.reject(error);
    });
    return d.promise();
}
Bluetooth.prototype.isMock = function() {
    return false;
}
Bluetooth.prototype.translate = function(codeblock) {
    function transOpcode(opcode) {
        if (opcode === 'FWD' || opcode === 'BACK') {
            return {cmd: 'm', pos: opcode === 'FWD'};
        } else if (opcode === 'TL' || opcode === 'TR') {
            return {cmd: 't', pos: opcode === 'TR'};
        }
    }
    function transValue(value) {
        var hex = value.toString(16);
        for (var p = 0, l = 4 - hex.length; p < l; p++) {
            hex = '0' + hex;
        }
        if (value < 0) {
            return hex;
        }
        return hex;
    }

    var nCode = transOpcode(codeblock.getOptions().opcode);
    var v = parseInt(codeblock.getOptions().data[0]);
    if (v < 0) {
        v *= -1;
        nCode.pos ^= true;
    }
    var nVal = transValue(v);

    return nCode.cmd + (nCode.pos ? '+' : '-') + nVal;
};