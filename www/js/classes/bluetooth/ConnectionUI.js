function ConnectionUI() {
    var div = document.createElement('div');
    div.id = 'connectionUI';
    document.body.appendChild(div);
    var elHolder = $(div).css({
        position: 'absolute',
        display: 'table',
        width: '100%',
        height: '100%'
    });
    elHolder.hide();
    this.el = $(document.createElement('div'));
    this.el.addClass('view');
    this.el.css({
//        display: 'table-cell',
        'vertical-align': 'middle',
        'background-color': 'rgba(0, 0, 0, 0.8)',
        'border-radius': '10px',
        'border': '3px solid rgba(255, 255, 255, 0.72)',
        'margin-left': 'auto',
        'margin-right': 'auto',
        'margin-top': '150px',
        'height': 'auto'
    });
    elHolder.append(this.el);

    var header = $(document.createElement('h1'));
    header.html('Bluetooth devices');
    header.css('text-align', 'center');
    this.el.append(header);
    this.list = $(document.createElement('ul'));
    this.el.append(this.list);

    this.el.on('click', 'li.device', function() {
        var t = $(this);
        console.debug('you selected ' + t.data('address'), t.html());
        Device.get().getBluetooth().connect(t.html()).done(function(){
            var event = document.createEvent('HTMLEvents');
            event.initEvent('bluetoothConnected', true, true);
            document.dispatchEvent(event);
            ConnectionUI.get().hide();
        }).fail(function(msg){
            alert('Connected with '+t.html()+' failed: '+msg);
        });
    });

    var close = $(document.createElement('div'));
    close.addClass('codeblock').css({
        'background-color': '#00f',
        'color': '#fff',
        'width': '200px',
        'text-align': 'center',
        'margin-left': 'auto',
        'margin-right': 'auto',
        'margin-top': '20px'
    });
    var span = $(document.createElement('span'));
    span.html('Close').css({
        color: '#fff',
        top: '5px',
        position: 'relative',
        'font-size': '23px'
    });
    close.append(span);
    this.el.append(close);
    close.on('click', function() {
        this.el.parent().hide();
    }.bind(this));

}
ConnectionUI.prototype.show = function() {
    this.list.empty();
    var searchLi = $(document.createElement('li'));
    searchLi.css({
        'background-color': '#ff0',
        'border-radius': '5px',
        'border': '1px solid #111',
        'text-align': 'center',
        'color': '#000',
        'padding': '10px'
    }).html('Searching');
    this.list.append(searchLi);
    this.el.parent().show();

    Device.get().getBluetooth().getDevices().done(function(devices) {
        var nn = searchLi.clone();
        this.list.empty();
        for (var i = 0; i < devices.length; i++) {
            var d = nn.clone();
            d.data('address', devices[i].address);
            d.html(devices[i].name);
            d.addClass('device');
            d.css({
                'background-color': '#0f0'
            });
            this.list.append(d);
        }
    }.bind(this));
}
ConnectionUI.prototype.hide = function() {
    this.el.parent().hide();
}
ConnectionUI.instance = undefined;
ConnectionUI.get = function() {
    if (typeof ConnectionUI.instance === 'undefined') {
        ConnectionUI.instance = new ConnectionUI();
    }
    return ConnectionUI.instance;
}