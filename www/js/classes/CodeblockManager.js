function CodeblockManager() {
    this.getAvailableBlocks = function() {
        return CodeblockDefinitions.blocks.slice(0);
    };
    this.getBlockByID = function(id) {
        var availableBlocks = this.getAvailableBlocks();
        for (var i = 0; i < availableBlocks.length; i++) {
            var block = availableBlocks[i];
            if (block.getOptions().id === id)
                return new Codeblock(block.getOptions());
        }
        return undefined;
    };
    this.getBlockByCommand = function(command) {
        var data = command.split(' ');
        var opcode = data.splice(0, 1)[0];
        var availableBlocks = this.getAvailableBlocks();
        for (var i = 0; i < availableBlocks.length; i++) {
            var block = availableBlocks[i];
            if (block.getOptions().opcode === opcode) {
                var nB = new Codeblock(block.getOptions());
                nB.getOptions().data = data;
                return nB;
            }
        }
        return undefined;
    };
}
CodeblockManager.prototype.insert = function(container, block, draggable) {
    container.append(block.getHTML());
    if (typeof draggable !== 'undefined') {
        container.find('.codeblock:last').each(function(index, element) {
            $(element).draggable(draggable);
        });
    }
}
CodeblockManager.prototype.insertAll = function(container, draggable) {
    var that = this;
    draggable = typeof draggable !== 'undefined' ? draggable : {
        helper: 'clone',
        cursorAt: {buttom: 0},
        snap: false,
        zIndex: 100
    };
    this.getAvailableBlocks().forEach(function(block) {
        that.insert(container, block, draggable);
    });
}
CodeblockManager.instance = undefined;
CodeblockManager.get = function() {
    if (typeof CodeblockManager.instance === 'undefined') {
        CodeblockManager.instance = new CodeblockManager();
    }
    return CodeblockManager.instance;
};