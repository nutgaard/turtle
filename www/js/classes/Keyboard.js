function Keyboard(options) {
    fabric.Object.prototype.originX = "center";
    fabric.Object.prototype.originY = "center";
    var options = Options.get().getOptions().keyboard;
    options.width = Math.min($(document).width(), 700);
    var focusElement;
    var keyboardCanvas = document.getElementById('onScreenKeyboard');
    var keyboardCanvas = document.createElement('canvas');
    keyboardCanvas.id = 'onScreenKeyboard';
    keyboardCanvas.className = 'view';
    keyboardCanvas.width = options.width;
    keyboardCanvas.height = options.height;
    
    var keyboardHolder = $(document.createElement('div')).attr('id', 'keyboardHolder');
    keyboardHolder.append(keyboardCanvas);
    document.body.appendChild(keyboardHolder[0]);
    var canvas = new fabric.Canvas('onScreenKeyboard');
    canvas.selection = false;
    var canvasContainer = $(keyboardCanvas).parent().css({
        'margin-left': 'auto',
        'margin-right': 'auto',
        'margin-bottom': '-' + (options.height + 20) + 'px'
    }); 
    console.debug('container', canvasContainer);
    // HIDESMALL HIDELARGE ABC 123 DELSMALL DELLARGE A-Z 0-9 +-*/
    var letterButtons = [];
    var base;
    var digitButtons = [];
    var currentButtons = [];
    function combine() {
        var position = [].splice.call(arguments, 0, 1);
        var elements = [].slice.call(arguments, 0);
        var g = new fabric.Group(elements, position[0]);
        g.set('selectable', false);
        return g;
    }
    function getActive() {
        return $(document.activeElement);
    }
    function createBase() {
        base = combine({}, new fabric.Rect({
            top: options.height / 2 + options.rounding,
            left: options.width / 2,
            width: options.width,
            height: options.height + 2 * options.rounding,
            fill: options.borderColor,
            rx: options.rounding,
            ry: options.rounding
        }), new fabric.Rect({
            top: options.height / 2 + options.rounding,
            left: options.width / 2,
            width: options.width - 2 * options.border,
            height: options.height - 2 * options.border + 2 * options.rounding,
            fill: options.bgColor,
            rx: options.rounding,
            ry: options.rounding
        }));
        canvas.add(base);
    }
    function createLetterButtons() {
        var workingWidth = options.width - 2 * options.border,
                workingHeight = options.height - options.border;

        var letterSize = {
            width: (workingWidth - 7 * options.border) / 6,
            height: (workingHeight - 6 * options.border - options.rounding) / 6
        };
        letterButtons.push(combine({
            top: options.height - letterSize.height / 2,
            left: options.width / 2

        }, new fabric.Rect({
            width: options.width - 2 * options.border,
            height: letterSize.height + 2 * options.rounding,
            fill: options.bgColor,
            stroke: options.borderColor,
            strokeWidth: 2,
            rx: options.rounding,
            ry: options.rounding
        }), new fabric.Line([-options.width / 4, -letterSize.height / 4, 0, letterSize.height / 4], {
            stroke: options.borderColor,
            strokeWidth: 5
        }), new fabric.Line([options.width / 4, -letterSize.height / 4, 0, letterSize.height / 4], {
            stroke: options.borderColor,
            strokeWidth: 5
        })));
        var letters = "ABCDEFGHIJKLMNOQRSTUVWXYZ".split('');
        var letterInd = 0;
        for (row = 0; row < 5; row++) {
            for (col = 0; col < 6; col++) {
                if (letterInd === letters.length)
                    break;
                letterButtons.push(combine({
                    top: letterSize.height / 2 + options.rounding + (options.border + letterSize.height) * row,
                    left: letterSize.width / 2 + 2 * options.border + (options.border + letterSize.width) * col
                }, new fabric.Rect({
                    width: letterSize.width,
                    height: letterSize.height,
                    fill: options.buttonColor,
                    stroke: options.borderColor,
                    strokeWidth: 2,
                    rx: options.rounding / 2,
                    ry: options.round / 2
                }), new fabric.Text(letters[letterInd++], {})));
            }
        }
        letterButtons.push(combine({
            top: letterSize.height / 2 + options.rounding + (options.border + letterSize.height) * 4,
            left: letterSize.width / 2 + 2 * options.border + (options.border + letterSize.width) * 5
        }, new fabric.Rect({
            width: letterSize.width,
            height: letterSize.height,
            fill: options.buttonColor,
            stroke: options.borderColor,
            strokeWidth: 2,
            rx: options.rounding / 2,
            ry: options.round / 2
        }), new fabric.Text('123', {})));
        letterButtons.push(combine({
            top: letterSize.height / 2 + options.rounding + (options.border + letterSize.height) * 4,
            left: options.width / 2
        }, new fabric.Rect({
            width: 4 * letterSize.width + 3 * options.border,
            height: letterSize.height,
            fill: options.buttonColor,
            stroke: options.borderColor,
            strokeWidth: 2,
            rx: options.rounding / 2,
            ry: options.round / 2
        }), new fabric.Text('DEL', {})));
    }
    function createDigitButtons() {
        var workingWidth = options.width - 2 * options.border,
                workingHeight = options.height - options.border;

        var digitSize = {
            width: (workingWidth - 5 * options.border) / 4,
            height: (workingHeight - 5 * options.border - options.rounding) / 5
        };
        digitButtons.push(combine({
            top: options.height - digitSize.height / 2,
            left: options.width / 2

        }, new fabric.Rect({
            width: options.width - 2 * options.border,
            height: digitSize.height + 2 * options.rounding,
            fill: options.bgColor,
            stroke: options.borderColor,
            strokeWidth: 2,
            rx: options.rounding,
            ry: options.rounding
        }), new fabric.Line([-options.width / 4, -digitSize.height / 4, 0, digitSize.height / 4], {
            stroke: options.borderColor,
            strokeWidth: 5
        }), new fabric.Line([options.width / 4, -digitSize.height / 4, 0, digitSize.height / 4], {
            stroke: options.borderColor,
            strokeWidth: 5
        })));
        var digits = ['1', '2', '3', '+', '4', '5', '6', '-', '7', '8', '9', '*', 'DEL', 'ABC', '0', '/'];
        var digitInd = 0;
        for (row = 0; row < 4; row++) {
            for (col = 0; col < 4; col++) {
                if (digitInd === digits.length)
                    break;
                digitButtons.push(combine({
                    top: digitSize.height / 2 + options.rounding + (options.border + digitSize.height) * row,
                    left: digitSize.width / 2 + 2 * options.border + (options.border + digitSize.width) * col
                }, new fabric.Rect({
                    width: digitSize.width,
                    height: digitSize.height,
                    fill: options.buttonColor,
                    stroke: options.borderColor,
                    strokeWidth: 2,
                    rx: options.rounding / 2,
                    ry: options.round / 2
                }), new fabric.Text(digits[digitInd++], {})));
            }
        }
    }
    createBase();
    createLetterButtons();
    createDigitButtons();
    hideKeyboard();
    $(document).on('focus', 'input[type=text]', function(event) {
        event.preventDefault();
        focusElement = $(event.target);
        console.debug('target set to ', focusElement);

        showKeyboard();
    });
    $(document).on('blur', 'input[type=text]', function(event) {
        event.preventDefault();
        focusElement = undefined;

        //Hacky way of allowing focus event to be handled before blur to avoid jumping keyboard
        setTimeout(function() {
            console.debug('Target set to empty', getActive());
            if (!getActive().is('input')) {
                hideKeyboard();
            }
        }, 0);
    });
    $(document).on('mousedown', '.canvas-container', function(event) {
        //Preventing the loss of focus if keyboard is touched.
        event.preventDefault();
    });
    $(document).on('change', 'input[type=text]', function(event) {
        event.preventDefault();
    })
    canvas.on('mouse:up', function(event) {
        if (typeof event.target === 'undefined' || typeof event.target._objects === 'undefined' || event.target._objects.length < 2)
            return;
        var letter = event.target._objects[1].text || 'HIDE';
        if (letter.length === 1) {
            //normal input
            focusElement.val(focusElement.val() + letter);
        } else {
            //needs special care, this one
            if (letter === 'DEL') {
                var c = focusElement.val();
                focusElement.val(c.substr(0, c.length - 1));
            } else if (letter === 'ABC') {
                showKeyboard('alpha');
            } else if (letter === '123') {
                showKeyboard('num');
            } else if (letter === 'HIDE') {
                hideKeyboard();
            } else {
                alert('Please contact a teacher, and explain what you were doing?');
            }
        }
    });
    function showKeyboard(kb) {
        if (typeof focusElement === 'undefined')
            return;
        currentButtons.forEach(function(btn) {
            canvas.remove(btn);
        });
        currentButtons = [];
        if (typeof kb === 'undefined') {
            if (focusElement.hasClass('number')) {
                kb = digitButtons;
            } else {
                kb = letterButtons;
            }
        } else {
            if (kb === 'num') {
                kb = digitButtons;
            }
            else {
                kb = letterButtons;
            }
        }
        currentButtons = kb.slice(0);
        if (focusElement.hasClass('nochange')) {
            if (kb === letterButtons) {
                currentButtons.splice(currentButtons.length-2, 1);
            }else {
                currentButtons.splice(currentButtons.length-3, 1);
            }
        }
        //No need for the ability to change to letters if variables are deactived
        if (kb === digitButtons && !Options.get().getOptions().extendedMode) {
            currentButtons.splice(currentButtons.length-3, 1);
        }
        
        currentButtons.forEach(function(btn) {
            canvas.add(btn);
        });
        console.debug('animateShow');
        canvasContainer.show()  .animate({
            'margin-bottom': '0px'
        }, 'slow', function() {
            console.debug('calc offset');
            canvas.calcOffset();
        });
    }
    function hideKeyboard() {
        console.debug('animateHide');
        if (typeof focusElement !== 'undefined') {
            focusElement.blur();
        }
        canvasContainer.animate({
            'margin-bottom': '-' + (options.height + 20) + 'px'
        }, 'slow', function() {
            console.debug('calc offset');
            canvas.calcOffset();
            $(this).hide();
        })
    }
}