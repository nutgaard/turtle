function JustTraceSimulator(htmlCanvas) {
    console.debug('recreating canvas');
    fabric.Object.prototype.originX = "center";
    fabric.Object.prototype.originY = "center";
    fabric.Object.selectable = false;
    fabric.Object.hasControls = false;
    fabric.Object.hasBorders = false;
    fabric.Object.hasRotatingPoint = false;
    //Canvas must have width/height attributes hardcoded to avoid being resized
    var width = htmlCanvas.width();
    var height = htmlCanvas.height();
    htmlCanvas.attr('width', width);
    htmlCanvas.attr('height', height);
    var id = htmlCanvas.attr('id');
    var canvas = this.__canvas = new fabric.StaticCanvas(id);
    canvas.renderOnAddRemove=false;
    var options = Options.get().getOptions().simulation;
    canvas.setBackgroundColor(options.bordercolor);
    var background = new fabric.Rect({
        top: height / 2,
        left: width / 2,
        width: canvas.width - 2 * options.border,
        height: canvas.height - 2 * options.border,
        fill: options.backgroundcolor
    });
    var abortCondition = false;
    canvas.add(background);
    canvas.renderAll();
    this.reset = function() {
        abortCondition = false;
    }
    this.abort = function() {
        abortCondition = true;
    }
    this.run = function(label, program) {
        this.reset();
        var myRobot = {
            angle: 0,
            left: canvas.width / 2,
            top: canvas.height / 2
        }
        var lastMove;
        function createTrace(blocks, opts) {
            var block = blocks.next();
            if (typeof block === 'undefined') {
                label.html('Error in code');
                return;
            }
            lastMove = {start: clone(myRobot)};
            block.applyTo(myRobot);
            lastMove.end = clone(myRobot);
            var t = new fabric.Rect({
                top: lastMove.start.top - (lastMove.start.top - lastMove.end.top) / 2,
                left: lastMove.start.left - (lastMove.start.left - lastMove.end.left) / 2,
                height: Math.sqrt(Math.pow(lastMove.start.top - lastMove.end.top, 2) + Math.pow(lastMove.start.left - lastMove.end.left, 2)),
                width: options.robot.robotsize * 0.3,
                fill: options.tracecolorDone,
                angle: lastMove.start.angle
            });
            canvas.add(t);
            if (blocks.hasNext() && !abortCondition) {
//                setTimeout(function(){createTrace(blocks, opts)}.bind(this),0);
                createTrace(blocks, opts);
            } else {
                var l = abortCondition ? 'Run' : 'Rerun';
                label.html(l);
                label.parent().css('background-color', '#0f0');
                canvas.renderAll();
            }
        }
        var generator = program.getGenerator();
        if (!generator.hasNext()) {
            return;
        }
        label.html('Abort');
        label.parent().css('background-color', '#f00');

        setTimeout(function() {
            createTrace(generator, options)
        }.bind(this), 0);
    }
}