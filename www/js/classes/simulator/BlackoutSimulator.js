function BlackoutSimulator(htmlCanvas) {
    fabric.Object.prototype.originX = "center";
    fabric.Object.prototype.originY = "center";
    //Canvas must have width/height attributes hardcoded to avoid being resized
    var width = htmlCanvas.width();
    var height = htmlCanvas.height();
    htmlCanvas.attr('width', width);
    htmlCanvas.attr('height', height);
    var id = htmlCanvas.attr('id');
    var canvas = this.__canvas = new fabric.StaticCanvas(id);
    canvas.setBackgroundColor('#111');
    var abortCondition = false;
    var timer;
    var text = new fabric.Text('Running program', {
        fontFamily: 'Rokkitt',
        fill: '#fff',
        fontSize: 32,
        top: canvas.height/2,
        left: canvas.width/2
    });
    this.abort = function() {
        abortCondition = true;
        clearTimeout(timer);
        var l = abortCondition ? 'Run' : 'Rerun';
        text.set({text: 'Program aborted'});
        canvas.renderAll();
        canvas.renderAll();
        $('#run').children().first().html(l);
        $('#run').css('background-color', '#0f0');
    }
    this.run = function(label, program) {
        function animate(blocks, opts) {
            var block = blocks.next();
            if (typeof block === 'undefined') {
                label.html('Error in code');
                return;
            }
            if (Device.get().hasBluetooth()) {
                var bluetooth = Device.get().getBluetooth();
                bluetooth.isConnected().done(function(status) {
                    if (status) {
                        cmdSend = bluetooth.send(block);
                        cmdSend.fail(function() {
                            SimulatorFactory.get().getCurrentSimulator().abort();
                            alert('Bluetooth communication failed');
                        }.bind(this));
                    } else {
                        console.debug('Device not connected, just go ahead');
                    }
                });
            }
            timer = setTimeout(function() {
                previousBlock = block;
                if (blocks.hasNext() && !abortCondition) {
                    animate(blocks, opts);
                } else {
                    var l = abortCondition ? 'Run' : 'Rerun';
                    label.html(l);
                    label.parent().css('background-color', '#0f0');
                    text.set({text: 'Program complete'});
                }
                //render a few time to allow android to work as well
                canvas.renderAll();
                canvas.renderAll();
            }, block.animationLength());
        }
        var generator = program.getGenerator();
        if (!generator.hasNext())
            return;
        label.html('Abort');
        label.parent().css('background-color', '#f00');
        text.set({text: 'Running'});
        canvas.add(text);
        animate(generator, options);
    };
}