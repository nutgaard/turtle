function BluetoothSimulator(htmlCanvas) {
    fabric.Object.prototype.originX = "center";
    fabric.Object.prototype.originY = "center";
    //Canvas must have width/height attributes hardcoded to avoid being resized
    var width = htmlCanvas.width();
    var height = htmlCanvas.height();
    htmlCanvas.attr('width', width);
    htmlCanvas.attr('height', height);
    var id = htmlCanvas.attr('id');
    var canvas = this.__canvas = new fabric.StaticCanvas(id);
//    var options = Options.get().getOptions().simulation;
    var options = {
        headsupDisplay: {
            width: 300,
            height: 180,
            corners: 20
        },
        border: 0
    }
    var timer = undefined;
    canvas.setBackgroundColor('#111');

    var headsupDisplay = [];
    var previousBlock = undefined;
    var abortCondition = false;
    var gradientOverlayCache = undefined;
    gradientOverlayCache = createGradientCache({
        type: 'linear',
        x1: 0,
        y1: -options.headsupDisplay.height / 2,
        x2: 0,
        y2: options.headsupDisplay.height / 2,
        colorStops: {
            0: "#ffffff",
            0.5: "#ffffff",
            1: "#7c7c7c"
        }});
    function createGradientCache(gradientO) {
        var c = document.createElement('canvas');
        c.width = options.headsupDisplay.width;
        c.height = options.headsupDisplay.height;
        var fabricHelper = new fabric.StaticCanvas(c);
        var shape = new fabric.Rect({
            width: options.headsupDisplay.width,
            height: options.headsupDisplay.height,
            top: options.headsupDisplay.height / 2,
            left: options.headsupDisplay.width / 2,
            rx: options.headsupDisplay.corners,
            ry: options.headsupDisplay.corners,
        });
        shape.setGradient('fill', gradientO);
        fabricHelper.add(shape);
        return c;
    }
    function createHeadsupImage(block, opts) {
        if (typeof block === 'undefined')
            return;
        fabric.Image.fromURL(gradientOverlayCache.toDataURL(), function(img) {
            var fontSize = 32;
            var text = new fabric.Text(block.getCode(), {
                fontFamily: 'Rokkitt',
                fill: block.getOptions().textColor,
                fontSize: fontSize
            });
            while (text.getBoundingRectWidth() + 2 * options.border > options.headsupDisplay.width) {
                text.set({
                    fontSize: --fontSize
                });
            }
            var filter = new fabric.Image.filters.Multiply({
                color: block.getOptions().color
            });
            img.filters.push(filter);
            img.applyFilters(canvas.renderAll.bind(canvas));
            var group = new fabric.Group([img, text], {
                top: opts.top,
                left: opts.left
            });
            if (typeof opts.opacity !== 'undefined') {
                group.set({
                    opacity: opts.opacity,
                });
            }
            headsupDisplay.push({block: block, display: group});
            canvas.add(group)
        });
    }
    this.reset = function() {
        headsupDisplay.forEach(function(e) {
            canvas.remove(e.display);
        });
        headsupDisplay = [];
        previousBlock = undefined;
        abortCondition = false;
        canvas.renderAll();
    };
    this.abort = function() {
        abortCondition = true;
        clearTimeout(timer);
        timer = undefined;
        var l = abortCondition ? 'Run' : 'Rerun';
        $('#run').children().first().html(l);
        $('#run').css('background-color', '#0f0');
    };
    this.run = function(label, program) {
        this.reset();
        var lastMove;
        function animate(blocks, opts) {
            var block = blocks.next();
            if (typeof block === 'undefined') {
                label.html('Error in code');
//                label.parent().css('background-color', '#0f0');
                return;
            }
//            console.debug('animating: ',block.getCode());
            //Headsup Display
            //Main
            createHeadsupImage(block, {
                top: canvas.height / 2,
                left: canvas.width / 2
            });
            //Next
            createHeadsupImage(blocks.peek(), {
                top: canvas.height / 2,
                left: canvas.width / 2 + options.headsupDisplay.width,
                fade: function(x, y, width, height) {
                    return 1 - (x / width);
                },
                opacity: 0.4
            });
            //Previous
            createHeadsupImage(previousBlock, {
                top: canvas.height / 2,
                left: canvas.width / 2 - options.headsupDisplay.width,
                fade: function(x, y, width, height) {
                    return x / width;
                },
                opacity: 0.4
            });

            if (Device.get().hasBluetooth()) {
                var bluetooth = Device.get().getBluetooth();
                bluetooth.isConnected().done(function(status) {
                    if (status) {
                        cmdSend = bluetooth.send(block);
                        cmdSend.fail(function() {
                            SimulatorFactory.get().getCurrentSimulator().abort();
                            alert('Bluetooth communication failed');
                        }.bind(this));
                    } else {
                        console.debug('Device not connected, just go ahead');
                    }
                });
            }
            var aniTime = block.animationLength();
            console.debug('Calculating time: ' + aniTime)
            if (block.getOptions().opcode === 'FWD' || block.getOptions().opcode === 'BACK') {
                aniTime /= 300 / 30;
                console.debug('Recalculated for robot: ' + aniTime);
            }
            timer = setTimeout(function() {
                previousBlock = block;
                if (blocks.hasNext() && !abortCondition) {
                    setTimeout(function() {
                        headsupDisplay.forEach(function(img) {
                            canvas.remove(img.display);
                        });
                        headsupDisplay = [];
                        animate(blocks, opts);
                    }, parseInt(Options.get().getOptions().timeout));
                } else {
                    var l = abortCondition ? 'Run' : 'Rerun';
                    label.html(l);
                    label.parent().css('background-color', '#0f0');
                    canvas.add(new fabric.Text('Program complete', {
                        top: canvas.height / 2,
                        left: canvas.width / 2,
                        fontFamily: 'Rokkitt',
                        fill: '#fff',
                        fontSize: 32
                    }));
                    headsupDisplay.forEach(function(img) {
                        canvas.remove(img.display);
                    });
                    headsupDisplay = [];
                }

                //render a few time to allow android to work as well
                canvas.renderAll();
                canvas.renderAll();
            }, aniTime);
        }
        var generator = program.getGenerator();
        if (!generator.hasNext())
            return;
        label.html('Abort');
        label.parent().css('background-color', '#f00');

        animate(generator, options);
    };
}
fabric.Image.filters.Multiply = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
    type: 'Multiply',
    initialize: function(options) {
        options = options || {};
        this.color = options.color || '#ff69b4';
    },
    applyTo: function(canvasEl) {
        var context = canvasEl.getContext('2d'),
                imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
                data = imageData.data,
                iLen = data.length, i,
                tintR, tintG, tintB,
                r, g, b,
                source;

        source = new fabric.Color(this.color).getSource();

        tintR = source[0];
        tintG = source[1];
        tintB = source[2];

        for (i = 0; i < iLen; i += 4) {
            r = data[i];
            g = data[i + 1];
            b = data[i + 2];

            // alpha compositing
            data[i] = (tintR * r) / 255;
            data[i + 1] = (tintG * g) / 255;
            data[i + 2] = (tintB * b) / 255;
        }

        context.putImageData(imageData, 0, 0);
    },
    toObject: function() {
        return extend(this.callSuper('toObject'), {
            color: this.color,
            opacity: this.opacity
        });
    }
});