function Simulation(htmlCanvas) {
    fabric.Object.prototype.originX = "center";
    fabric.Object.prototype.originY = "center";
    //Canvas must have width/height attributes hardcoded to avoid being resized
    var width = htmlCanvas.width();
    var height = htmlCanvas.height();
    htmlCanvas.attr('width', width);
    htmlCanvas.attr('height', height);
    var id = htmlCanvas.attr('id');
    var canvas = this.__canvas = new fabric.StaticCanvas(id);
    var options = Options.get().getOptions().simulation;
    canvas.setBackgroundColor(options.bordercolor);
    var background = new fabric.Rect({
        top: height / 2,
        left: width / 2,
        width: canvas.width - 2 * options.border,
        height: canvas.height - 2 * options.border,
        fill: options.backgroundcolor
    });
    var robot = createRobot(options.robot);
    var headsupDisplay = [];
    var previousBlock = undefined;
    var trace = [];
    var abortCondition = false;
    var gradientOverlayCache = undefined;
    var gradientTransparencyCache = undefined;
    canvas.add(
            background,
            robot
            );

    gradientOverlayCache = createGradientCache({
        type: 'linear',
        x1: 0,
        y1: -options.headsupDisplay.height / 2,
        x2: 0,
        y2: options.headsupDisplay.height / 2,
        colorStops: {
            0: "#ffffff",
            0.5: "#ffffff",
            1: "#7c7c7c"
        }});
    gradientTransparencyCache = createGradientCache({
        type: 'linear',
        x1: -options.headsupDisplay.width / 2,
        y1: 0,
        x2: options.headsupDisplay.width / 2,
        y2: 0,
        colorStops: {
            0: "rgba(0, 0, 0, 1.0)",
            1: "rgba(0, 0, 0, 0.0)"
        }});
    function createRobot(opts) {
        var base = new fabric.Circle({
            radius: opts.robotsize,
            fill: opts.robotcolor
        });
        var headingarrow = new fabric.Triangle({
            width: opts.robotsize,
            height: opts.robotsize * 0.5,
            fill: opts.headingcolor,
            top: -opts.robotsize * 0.5
        });
        var headingbase = new fabric.Rect({
            width: opts.robotsize * 0.2,
            height: opts.robotsize * 0.8,
            fill: opts.headingcolor,
            top: opts.robotsize * 0.15
        });
        var leftWheel = new fabric.Rect({
            width: opts.robotsize * 0.15,
            height: opts.robotsize * 0.8,
            fill: opts.wheelcolor,
            left: -opts.robotsize * 0.75,
            rx: opts.wheelrounding,
            ry: opts.wheelrounding
        });
        var rightWheel = new fabric.Rect({
            width: opts.robotsize * 0.15,
            height: opts.robotsize * 0.8,
            fill: opts.wheelcolor,
            left: opts.robotsize * 0.75,
            rx: opts.wheelrounding,
            ry: opts.wheelrounding
        });
        return new fabric.Group([base, headingarrow, headingbase, leftWheel, rightWheel], {
            left: canvas.width / 2,
            top: canvas.height / 2
        });
    }
    function createGradientCache(gradientO) {
        var c = document.createElement('canvas');
        c.width = options.headsupDisplay.width;
        c.height = options.headsupDisplay.height;
        var fabricHelper = new fabric.StaticCanvas(c);
        var shape = new fabric.Rect({
            width: options.headsupDisplay.width,
            height: options.headsupDisplay.height,
            top: options.headsupDisplay.height / 2,
            left: options.headsupDisplay.width / 2,
            rx: options.headsupDisplay.corners,
            ry: options.headsupDisplay.corners,
        });
        shape.setGradient('fill', gradientO);
        fabricHelper.add(shape);
        return c;
    }
    function createHeadsupImage(block, opts) {
        if (typeof block === 'undefined')
            return;
        fabric.Image.fromURL(gradientOverlayCache.toDataURL(), function(img) {
            var fontSize = 20;
            var text = new fabric.Text(block.getCode(), {
                fontFamily: 'Rokkitt',
                fill: block.getOptions().textColor,
                fontSize: fontSize
            });
            while (text.getBoundingRectWidth() + 2 * options.border > options.headsupDisplay.width) {
                text.set({
                    fontSize: --fontSize
                });
            }
            var filter = new fabric.Image.filters.Multiply({
                color: block.getOptions().color
            });
            img.filters.push(filter);
            if (typeof opts.fade !== 'undefined') {
                var transFilter = new fabric.Image.filters.Transparency({
                    opacityFunction: opts.fade
                });
//                img.filters.push(transFilter);
            }
            img.applyFilters(canvas.renderAll.bind(canvas));
            var group = new fabric.Group([img, text], {
                top: opts.top,
                left: opts.left
            });
            if (typeof opts.opacity !== 'undefined') {
                group.set({
                    opacity: opts.opacity,
                });
            }
            headsupDisplay.push({block: block, display: group});
            canvas.add(group)
        });
    }
    this.reset = function() {
        robot.set({
            left: canvas.width / 2,
            top: canvas.height / 2,
            angle: 0
        });
        trace.forEach(function(e) {
            canvas.remove(e);
        });
        headsupDisplay.forEach(function(e) {
            canvas.remove(e.display);
        });
        trace = [];
        headsupDisplay = [];
        previousBlock = undefined;
        robot.setOpacity(1.0);
        abortCondition = false;
        canvas.renderAll();
    };
    this.abort = function() {
        abortCondition = true;
    };
    this.run = function(label, program) {
        this.reset();
        var myRobot = {
            angle: 0,
            left: robot.getLeft(),
            top: robot.getTop()
        };
        var lastMove;
        function animate(blocks, opts) {
            var block = blocks.next();
            if (typeof block === 'undefined') {
                label.html('Error in code');
//                label.parent().css('background-color', '#0f0');
                return;
            }
//            console.debug('animating: ',block.getCode());
            lastMove = {start: clone(myRobot)}
            block.applyTo(myRobot);
            lastMove.end = clone(myRobot);
            var localtrace = new fabric.Rect({
                top: robot.getTop(),
                left: robot.getLeft(),
                width: options.robot.robotsize * 0.3,
                height: options.robot.robotsize * 0.25,
                fill: options.tracecolorActive,
                angle: lastMove.start.angle
            });
            //Headsup Display
            //Main
            createHeadsupImage(block, {
                top: options.headsupDisplay.height / 2 + 2 * options.border,
                left: canvas.width / 2
            });
            //Next
            createHeadsupImage(blocks.peek(), {
                top: options.headsupDisplay.height / 2 + options.border,
                left: canvas.width / 2 + options.headsupDisplay.width,
                fade: function(x, y, width, height) {
                    return 1 - (x / width);
                },
                opacity: 0.4
            });
            //Previous
            createHeadsupImage(previousBlock, {
                top: options.headsupDisplay.height / 2 + options.border,
                left: canvas.width / 2 - options.headsupDisplay.width,
                fade: function(x, y, width, height) {
                    return x / width;
                },
                opacity: 0.4
            });
            canvas.add(localtrace);
            canvas.bringToFront(robot);

            if (Device.get().hasBluetooth()) {
                var bluetooth = Device.get().getBluetooth();
                bluetooth.isConnected().done(function(status) {
                    if (status) {
                        cmdSend = bluetooth.send(block);
                        cmdSend.fail(function() {
                            SimulatorFactory.get().getCurrentSimulator().abort();
                            alert('Bluetooth communication failed');
                        }.bind(this));
                    }else {
                        console.debug('Device not connected, just go ahead');
                    }
                });

            }
            robot.animate(myRobot, {
                duration: block.animationLength(),
                easing: function(t, b, c, d) {//currentTime, startValue, byValue, duration
                    return c * t / d + b;//linear movement
                },
                onChange: function() {
                    localtrace.set({
                        top: lastMove.start.top - (lastMove.start.top - robot.getTop()) / 2,
                        left: lastMove.start.left - (lastMove.start.left - robot.getLeft()) / 2,
                        height: Math.sqrt(Math.pow(lastMove.start.top - robot.getTop(), 2) + Math.pow(lastMove.start.left - robot.getLeft(), 2))
                    });
                    canvas.renderAll();
                },
                onComplete: function() {
                    previousBlock = block;
                    localtrace.set({
                        fill: options.tracecolorDone
                    });
                    trace.push(localtrace);
                    localtrace = undefined;
                    if (blocks.hasNext() && !abortCondition) {
                        animate(blocks, opts);
                    } else {
                        robot.setOpacity(0.65);
                        var l = abortCondition ? 'Run' : 'Rerun';
                        label.html(l);
                        label.parent().css('background-color', '#0f0');
                    }
                    headsupDisplay.forEach(function(img) {
                        canvas.remove(img.display);
                    });
                    headsupDisplay = [];
                    //render a few time to allow android to work as well
                    canvas.renderAll();
                    canvas.renderAll();
                },
                abort: function() {
                    return abortCondition;
                }
            });
        }
        var generator = program.getGenerator();
        if (!generator.hasNext())
            return;
        label.html('Abort');
        label.parent().css('background-color', '#f00');

        animate(generator, options);
    };
}
fabric.Image.filters.Multiply = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
    type: 'Multiply',
    initialize: function(options) {
        options = options || {};
        this.color = options.color || '#ff69b4';
    },
    applyTo: function(canvasEl) {
        var context = canvasEl.getContext('2d'),
                imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
                data = imageData.data,
                iLen = data.length, i,
                tintR, tintG, tintB,
                r, g, b,
                source;

        source = new fabric.Color(this.color).getSource();

        tintR = source[0];
        tintG = source[1];
        tintB = source[2];

        for (i = 0; i < iLen; i += 4) {
            r = data[i];
            g = data[i + 1];
            b = data[i + 2];

            // alpha compositing
            data[i] = (tintR * r) / 255;
            data[i + 1] = (tintG * g) / 255;
            data[i + 2] = (tintB * b) / 255;
        }

        context.putImageData(imageData, 0, 0);
    },
    toObject: function() {
        return extend(this.callSuper('toObject'), {
            color: this.color,
            opacity: this.opacity
        });
    }
});

fabric.Image.filters.Transparency = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
    type: 'Transparency',
    initialize: function(options) {
        options = options || {};
        this.opacityFunction = options.opacityFunction || function(x, y, width, height) {
            return (155 - x / width * 155) / 255;
        };
    },
    applyTo: function(canvasEl) {
        var context = canvasEl.getContext('2d'),
                imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
                data = imageData.data,
                iLen = data.length,
                height = canvasEl.height,
                width = canvasEl.width,
                alphaInd;

        for (var y = 0; y < height; y++) {
            for (x = 0; x < width; x++) {
                var alphaInd = ((width * y) + x) * 4 + 3;
                var factor = Math.max(0, Math.min(1, this.opacityFunction(x, y, width, height)));
                data[alphaInd] = data[alphaInd] * factor;
            }
        }
        context.putImageData(imageData, 0, 0);
    },
    toObject: function() {
        return extend(this.callSuper('toObject'), {
            opacityFunction: this.opacityFunction
        });
    }
});