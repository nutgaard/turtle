function SimulatorFactory() {
    function getStrategy() {
        var strategy = Options.get().getOptions().simulatorStrategy;
        if (strategy === 'alwaysSimulator') {
            return {
                normal: {type: 1, simulator: 'Simulation'},
                bluetooth: {type: 1, simulator: 'Simulation'}
            };
        } else if (strategy === 'bluetoothAdaptiveHeadsup') {
            return {
                normal: {type: 1, simulator: 'Simulation'},
                bluetooth: {type: 2, simulator: 'BluetoothSimulator'}
            };
        } else if (strategy === 'bluetoothAdaptiveBlackout') {
            return {
                normal: {type: 1, simulator: 'Simulation'},
//                normal: {type: 3, simulator: 'BlackoutSimulator'},
                bluetooth: {type: 3, simulator: 'BlackoutSimulator'}
            };
        } else if (strategy === 'justTraceSimulator') {
            return {
//                normal: {type: 1, simulator: 'Simulation'},
                normal: {type: 4, simulator: 'JustTraceSimulator'},
                bluetooth: {type: 4, simulator: 'JustTraceSimulator'}
            };
        }
    }
    var device = Device.get();
    var currentSimulation = {type: 0, simulator: undefined};

    this.hasBluetoothConnection = false;

    var connectionProbe = undefined;

    this.stopConnectionProbe = function() {
        clearInterval(connectionProbe);
        connectionProbe = undefined;
        return this;
    };
    this.startConnectionProbe = function() {
        if (typeof connectionProbe !== 'undefined')
            return this;
        connectionProbe = setInterval(function() {
            Device.get().getBluetooth().isConnected().done(function(state) {
                console.debug('BluetoothConnection: ', state);
                this.hasBluetoothConnection = state;
            }.bind(this));
        }.bind(this), 1000);
        return this;
    };
    this.getCurrentSimulator = function() {
        return currentSimulation;
    }
    this.getSimulatorForCanvas = function(canvas) {
        var strategy = getStrategy();
        console.debug('strategy: ', strategy);
        if (this.hasBluetoothConnection) {
            strategy = strategy.bluetooth;
            
        } else {
            strategy = strategy.normal;
        }
        currentSimulation = {type: strategy.type, simulator: new window[strategy.simulator](canvas)};
        return currentSimulation.simulator;
    };
    this.startConnectionProbe();
}
SimulatorFactory.instance = undefined;
SimulatorFactory.get = function() {
    if (typeof SimulatorFactory.instance === 'undefined') {
        SimulatorFactory.instance = new SimulatorFactory();
    }
    return SimulatorFactory.instance;
};
