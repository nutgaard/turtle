function Device(debug) {
    var hasDevice = false;
    var bluetooth = debug ? new MockBluetooth() : undefined;
    var listeners = [];
    document.addEventListener('deviceready', function() {
        hasDevice = true;
        alert('on device');
        bluetooth = bluetooth || new Bluetooth();
        if (typeof listener !== 'undefined') {
            listeners.forEach(function(listener) {
                listener();
            });
        }
    });
    this.addListener = function(listener) {
        listeners.push(listener);
    };
    this.isDeviceDetected = function() {
        return hasDevice;
    };
    this.hasBluetooth = function() {
        return typeof bluetooth !== 'undefined';
    }
    this.getBluetooth = function() {
        return bluetooth;
    }
}
Device.instance = undefined;
Device.get = function(debug) {
    if (typeof Device.instance === 'undefined') {
        var debug = debug || false;
        Device.instance = new Device(debug);
    }
    return Device.instance;
}
