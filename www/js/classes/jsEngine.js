function jsEngine() {
    var translationTable = {
        'ICOUNT_START': 'var icount = 0;',
        'ICOUNT_TERMINATE_TEST': 'if (icount > ${0}){return;}',
        'ICOUNT_INCREMENT': 'icount++;',
        'FWD': 'FWD(${0});',
        'BACK': 'BACK(${0});',
        'TL': 'TL(${0});',
        'TR': 'TR(${0});',
        'SET': 'SET("${0}", ${1});var ${0} = ${1};',
//        'PROC': 'PROC("${0}");function ${0}() {',
        'PROC': 'function ${0}() {',
        'END': 'END();}',
        'CALL': 'CALL("${0}");${0}();',
        'REP': 'REP(${0});for (var ${gen} = 0; ${gen} < ${0};${gen}++) {'
    };
    this.getJS = function(program) {
        var f = [translationTable.ICOUNT_START];
        program.toGraphics().forEach(function(block) {
            var opcode = block.getOptions().opcode;
            var template = translationTable[opcode];
            if (CodeblockDefinitions.isActuator(block)) {
                f.push(translationTable.ICOUNT_INCREMENT);
                f.push(translationTable.ICOUNT_TERMINATE_TEST);
            }
            f.push(jsEngine.renderTemplate(template, block.getOptions().data));
        });
        var str = f.join(' ');
        return str;
    };
}
jsEngine.renderTemplate = function(template, data) {
    var genVals = {};
    return template.trim().replace(/\$\{(\d+|[a-z\d]+)\}/gi, function(match, backref) {
        if (backref.startsWith('gen')) {
            if (typeof genVals[backref] === 'undefined') {
                genVals[backref] = 'generated_' + (Math.round(Math.random() * 100000));
            }
            return genVals[backref]
        } else {
            return data[backref];
        }
    });
};
function jsEngineGenerator(js) {
    var program = [];
    var generated = false;
    var showNonActuators = false;
    this.interceptors = [];
    this.bindings = [];
    CodeblockDefinitions.blocks.forEach(function(block) {
        this.interceptors.push(function() {
            if (!showNonActuators && !CodeblockDefinitions.isActuator(block.getCode()))return;
            var args = Array.prototype.slice.call(arguments);
            program.push(block.getCode() + args.join(' ').trim());
        });
        this.bindings.push(block.getCode().trim());
    }.bind(this));
    
    
    
    this.getGenerator = function() {
        if (!generated) {
            this.generate(30000);
        }
        return new function() {
            var pc = 0;
            this.peek = function() {
                if (pc < program.length)
                    return CodeblockManager.get().getBlockByCommand(program[pc]);
                return undefined;
            };
            this.hasNext = function() {
                return typeof this.peek() !== 'undefined';
            };
            this.next = function() {
                if (!this.hasNext()) {
                    console.error('cannot call next() when hasNext() is false');
                    return undefined;
                }
                return CodeblockManager.get().getBlockByCommand(program[pc++]);
            };
        };
    };

    this.generate = function(nof_instructions) {
        pc = 0;
        program = [];
        generated = true;
        var compiled = jsEngine.renderTemplate(js, [nof_instructions]);
        console.debug('compiled JS-function: ', compiled);
        
        //Creating sandboxed environment for user inputted code
        var frame = document.createElement('iframe');
        document.body.appendChild(frame);
        
        var F = frame.contentWindow.Function;
        var shadows = Object.keys(frame.contentWindow).join();
        
        document.body.removeChild(frame);
        
//        var exec = new F(this.bindings.concat(shadows), compiled);
        var exec = new Function(this.bindings, compiled);
        exec.apply(this, this.interceptors);
    };

    this.getProgram = function() {
        return program.slice(0);
    };
}