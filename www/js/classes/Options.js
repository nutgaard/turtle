function Options() {
    var div = document.createElement('div');
    div.id = 'optionsView';
    document.body.appendChild(div);
    var elHolder = $(div).css({
        position: 'absolute',
        top: '10px',
        left: '10px',
        'background-color': 'rgba(0, 0, 0, 0.8)',
        'border-radius': '10px',
        border: '3px solid rgba(255, 255, 255, 0.72)',
        width: ($(document).width() - 20) + 'px',
        height: ($(document).height() - 20) + 'px'
    });
    elHolder.hide();
    var el = $(document.createElement('div'));
    el.addClass('view');
    el.css({'padding': '10px'});
    elHolder.append(el);
    //Defaults
    var defaultOptions = {
        codeblock: {
            id: -1,
            string: 'Text',
            color: '#ff0080',
            needsInput: [],
            data: [],
            turnSpeed: 90,
            moveSpeed: 30
        },
        extendedMode: false,
        showProcedureCalls: false,
        defaultViewport: '#program',
        customKeyboard: 'none',
        simulation: {
            border: 5,
            backgroundcolor: '#BBB',
            bordercolor: '#666',
            robot: {
                robotsize: 30,
                robotcolor: '#66f',
                headingcolor: '#fff',
                wheelcolor: '#000',
                wheelrounding: 4
            },
            headsupDisplay: {
                width: 80,
                height: 60,
                corners: 8
            },
            tracecolorActive: '#3f3',
            tracecolorDone: '#3a3'
        },
        keyboard: {
            height: 400,
            border: 5,
            rounding: 10,
            bgColor: '#333',
            borderColor: '#777',
            buttonColor: '#ccc',
        },
        simulatorStrategies: ['alwaysSimulator', 'bluetoothAdaptiveHeadsup', 'bluetoothAdaptiveBlackout', 'justTraceSimulator'],
        simulatorStrategy: 'bluetoothAdaptiveHeadsup',
        timeout: '1200',
        timeoutOptions: ['200', '600', '1200', '2000']
    }
    var optionsData = $.extend({}, defaultOptions, JSON.parse(window.localStorage.getItem('turtle')));

    this.load = function() {
        optionsData = $.extend({}, defaultOptions, JSON.parse(window.localStorage.getItem('turtle')));
    }
    this.save = function() {
        var str = JSON.stringify(optionsData);
        console.debug('saving: ', str);
        window.localStorage.setItem('turtle', str);
    }
    this.purge = function() {
        window.localStorage.removeItem('turtle');
        this.load();
    }
    this.getOptions = function() {
        return  optionsData;
    }
    this.createHTML = function() {
        function appendButton(text, color, listener) {
            var button = $(document.createElement('div'));
            button.addClass('codeblock').css({
                'background-color': color,
                'color': '#fff',
                'width': '200px',
                'text-align': 'center',
                'margin-left': 'auto',
                'margin-right': 'auto',
                'margin-top': '20px'
            });
            var span = $(document.createElement('span'));
            var textColor = findTextColor(color);
            span.html(text).css({
                'color': textColor,
                'top': '5px',
                'position': 'relative',
                'font-size': '23px'
            });
            button.append(span);
            el.append(button);
            button.on('click', listener);
        }
        function appendCheckbox(message, value, callback) {
            var p = $(document.createElement('p'));
            p.html(message);
            p.css({'padding': '10px'});

            var c = $(document.createElement('input'));
            c.attr('type', 'checkbox');
            c.prop('checked', value);
            p.append(c);
            el.append(p);
            c.on('change', callback);
        }
        function appendChoice(message, value, choices, callback) {
            var p = $(document.createElement('p'));
            p.html(message);
            p.css({'padding': '10px'});

            var c = $(document.createElement('select'));
            for (var i = 0; i < choices.length; i++) {
                var s = $(document.createElement('option'));
                s.html(choices[i]);
                if (choices[i] === value) {
                    s.prop('selected', true);
                }
                c.append(s);
            }
            c.on('change', callback);
            p.append(c);
            el.append(p);
        }
        var header = $(document.createElement('h1'));
        header.html('Settings');
        header.css({'text-align': 'center'});
        el.append(header);

        appendCheckbox('Use extended mode:', optionsData.extendedMode, function(event) {
            Options.get().getOptions().extendedMode = $(this).prop('checked') === true;
            console.debug('options', optionsData);
            Options.get().save();
            new CodeblockDefinitions();
            $('#header .buttons').children().remove();
            CodeblockManager.get().insertAll($('#header .buttons'));
        });
        appendCheckbox('Show precedure invokations:', optionsData.showProcedureCalls, function(event) {
            Options.get().getOptions().showProcedureCalls = $(this).prop('checked') === true;
            Options.get().save();
        });

        el.append(document.createElement('hr'));

        appendChoice('Default viewport:', optionsData.defaultViewport, ['#program', '#textualProgram'], function(event) {
            Options.get().getOptions().defaultViewport = $(this).find(':selected').text();
            Options.get().save();
        });
        appendChoice('Use custom smart keyboard:', optionsData.customKeyboard, ['none', 'device', 'all'], function(event) {
            Options.get().getOptions().customKeyboard = $(this).find(':selected').text();
            Options.get().save();
        });
        appendChoice('Simulator selection strategy: ', optionsData.simulatorStrategy, optionsData.simulatorStrategies, function(event){
            Options.get().getOptions().simulatorStrategy = $(this).find(':selected').text();
            Options.get().save();
        });
        appendChoice('Command timeout: ', optionsData.timeout, optionsData.timeoutOptions, function(event){
           Options.get().getOptions().timeout = $(this).find(':selected').text();
           Options.get().save();
        });

        el.append(document.createElement('hr'));
        if (Device.get().hasBluetooth()) {
            var bl = Device.get().getBluetooth();
            bl.isEnabled().done(function(status) {
                if (status) {
                    var cls = el.find('div.codeblock').detach();
                    appendButton('Connect', '#ff4', function() {
                        if ($(this).text() === 'Disconnect') {
                            bl.disconnect().fail(function(error) {
                                alert('could not disconnect from device: ' + error);
                            }).done(function() {
                                $(this).css('background-color', '#ff4').children().first().text('Connect');
                            }.bind(this));
                        } else {
                            ConnectionUI.get().show();
                        }
                    });
                    el.append(cls);
                }
            }.bind(this));
        }
        appendButton("Close", "#00f", function() {
            Options.get().toggle();
        });
    }
    $(document).on('bluetoothConnected', function() {
        el.find('div.codeblock').each(function(ind, ele) {
            if ($(ele).text() === 'Connect') {
                $(ele).css('background-color', '#0f0').children().first().text('Disconnect');
            }
        });
    });
}
Options.instance = undefined;
Options.get = function() {
    if (typeof Options.instance === 'undefined') {
        Options.instance = new Options();
    }
    return Options.instance;
}
Options.prototype.toggle = function() {
    var el = $('#optionsView');
    if (el.css('display') === 'none') {
        console.debug(el.children());
        if (el.find('.view').children().length === 0) {
            this.createHTML();
        }
        el.show();
    } else {
        el.hide();
    }
}