function Codeblock(options) {
    var opts = $.extend(true,{}, Options.get().getOptions().codeblock, options);
    if (typeof options.opcode === 'undefined') {
        opts.opcode = opts.string;
    }
    opts['textColor'] = findTextColor(opts.color);
    
    this.getHTML = function() {
        var o = '<li data-id="' + opts.id + '" class="codeblock wrapper" style="background-color: ' + opts.color + ';color: ' + opts.textColor + '"><div class="cell"><div class="content">';
        o += '<span>' + opts.string + '</span>';
        for (var i = 0; i < opts.needsInput.length; i++) {
            console.debug(opts.needsInput);
            var input = opts.needsInput[i];
            var readonly = $('#keyboardHolder').length >= 1 ? 'readonly="true"' : '';
            if (typeof opts.data !== 'undefined' && typeof opts.data[i] !== 'undefined') {
                o += '<input type="text" class="' + input.type + '" value="' + opts.data[i] + '" '+readonly+' />';
            } else {
                o += '<input type="text" class="' + input.type + '" placeholder="' + input.default + '" '+readonly+' />';
            }
        }
        o += '</div></div></li>';
        return o;
    };
    this.getOptions = function() {
        return opts;
    };
    this.getCode = function() {
        return String.format('{0} {1}', opts.opcode, opts.data.join(' '));
    };
    this.applyTo = function(robot) {
        return opts.move(robot, opts);
    };
    this.animationLength = function() {
        return opts.time(opts);
    }
}
Codeblock.fromHTML = function(element) {
    var id = parseInt(element.data('id'));
    if (id === -1) {
        console.debug('Invalid type parameter', id, 'for ', element);
        return;
    }
    var block = CodeblockManager.get().getBlockByID(id);
    var data = [];
    element.find('input').each(function(ind, ele) {
        var v = $(ele).val() || block.getOptions().needsInput[ind].default || 0;
        data.push(v);
    });
    block.getOptions().data = data;
    return block;
}