function RunnableProgram(program) {
    var commands = program;
    var engine = new jsEngine();
    var options = {
        showProcedureEnding: Options.get().getOptions().showProcedureCalls
    };
    this.getGenerator = function() {
        if (commands.getRaw()[0] === '//#javascript') {
            var rP = commands.getRaw();
            //Removing the comment line as everything is forces into one line,
            //and the inital line would make the whole program a comment.
            rP.splice(0, 1);
            var js = new jsEngineGenerator(rP.join(' '));
        } else {
            var js = new jsEngineGenerator(engine.getJS(commands));
        }
        return js.getGenerator();
    }
}