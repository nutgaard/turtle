if (!String.format) {
    String.format = function(format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] !== 'undefined'
                    ? args[number]
                    : match
                    ;
        });
    };
}
if (typeof String.prototype.startsWith !== 'function') {
    String.prototype.startsWith = function(str) {
        return this.slice(0, str.length) === str;
    };
}
if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(str) {
        return this.slice(-str.length) === str;
    };
}
function isArray(obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
}
if (!Array.prototype.peek) {
    Array.prototype.peek = function(back) {
        back = (back || 0) + 1;
        return this[this.length - back];
    }
}
function clone(obj) {
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj)
        return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr))
                copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}
Math.toDegrees = function(rads) {
    return rads * (180 / Math.PI);
}
Math.toRadians = function(degrees) {
    return degrees * (Math.PI / 180);
}
$.fn.reduce = [].reduce;
function debug(e) {
    console.debug(e);
    var d = $('#debug');
    var n = d.val() + '\n' + e;
    d.val(n);
}
function clampToRange(value, min, max) {
    if (value < min)
        return min;
    if (value >= max)
        return max - 1;
    return value;
}
function cloneCanvas(oldCanvas) {
    var newCanvas = document.createElement('canvas');
    var ctx = newCanvas.getContext('2d');

    newCanvas.width = oldCanvas.width;
    newCanvas.height = oldCanvas.height;

    ctx.drawImage(oldCanvas, 0, 0);

    return newCanvas;
}
function findTextColor(color) {
    if (color.charAt(0) === '#') {
        color = color.slice(1);
    } else {
        opts['color'] = '#' + color;
    }
    if (color.length === 3) {
        color = color.split('').map(function(a) {
            return a + a;
        }).join('')
    }
    if (color.length !== 6) {
        console.debug('Bad format on color', color);
        return '#ff0080';
    }
    var r = parseInt(color.substring(0, 2), 16);
    var g = parseInt(color.substring(2, 4), 16);
    var b = parseInt(color.substring(4, 6), 16);
    var brightness = (r * 299) + (g * 587) + (b * 114);
    brightness = brightness * 1.0 / 255000;
    return brightness >= 0.5 ? '#000' : '#fff';
}