['log', 'warn', 'debug'].forEach(function(method) {
  var old = console[method];
  console[method] = function() {
    var stack = (new Error()).stack.split(/\n/);
    // Chrome includes a single "Error" line, FF doesn't.
    if (stack[0].indexOf('Error') === 0) {
      stack = stack.slice(1);
    }
    var at = stack[1].trim().split(/[\s\.\/\:]/);
    var args = [[at[1], '::', at[at.length-2], ':'].join('')].concat([].slice.apply(arguments));
    
    return old.apply(console, [args.join(' ')]);
  };
});
$(document).ready(function() {
    Device.get(true);
    var options = Options.get().getOptions();
    setViewport(options.defaultViewport);
    if (options.defaultViewport !== '#program') {
        $('#modeChange span').html('G');
    }
    CodeblockManager.get();
    Device.get().addListener(function() {
        if (options.customKeyboard === 'device') {
            new Keyboard();
        }
        document.addEventListener('menubutton', toggleOptions);
        window.bluetooth = new Bluetooth();
    });

    if (options.customKeyboard === 'all') {
        new Keyboard();
    }
    debug('starting programming area');
    startSortable();
    debug('Creating listeners');
    createBindingsAndListeners();
    debug('Listeners created');
    CodeblockManager.get().insertAll($('#header .buttons'));

    SimulatorFactory.get().getSimulatorForCanvas($('#simulation'));
//    Simulation.get($('#simulation'));
    debug('Initialization complete. Happy programming');
    //This is just for testing purposes
//    sendTestEvent();
});

function startSortable() {
    var blockSize = {width: $('.start').outerWidth(), height: $('.start').outerHeight()};
    var startBlock = $('.start');
    var dropIndicator = $(document.createElement('li'));
    dropIndicator.addClass('drop');
    $('#program').bind('DOMNodeInserted', function(event) {
        var target = $(event.target);
        if (!target.is('.drop')) {
            target.draggable({
                helper: 'clone',
                snap: false,
                zIndex: 0
            });
        }
    })
    $(document).on('drag', '.codeblock', function(event, ui) {
        if (ui.offset.top < 60) {
            $('.drop').detach();
            return;
        }
        var elementsPerRow = Math.floor($('#program').width() / blockSize.width);
        var startOffset = startBlock.offset();

        var col = Math.floor((ui.offset.left - startOffset.left) / blockSize.width);
        var row = Math.floor((ui.offset.top - startOffset.top) / blockSize.height);
        col = clampToRange(col, 0, elementsPerRow);

        var nof_elements = $('#program li:not(.drop)').length;
        var completeRows = Math.floor(nof_elements / elementsPerRow);
        var activeRows = Math.ceil(nof_elements / elementsPerRow);

        if (completeRows === activeRows) {
            row = clampToRange(row, 0, completeRows + 1);
        } else {
            row = clampToRange(row, 0, activeRows);
        }
        var gridIndex = clampToRange(col + elementsPerRow * row, 0, nof_elements);
        var element = $('#program li:not(.drop):eq(' + gridIndex + ')');

//        console.debug('no need to change', dropIndicator.prev(), element);
        if (dropIndicator.prev()[0] === element[0]) {
            return;
        }
        dropIndicator.detach();
        element.after(dropIndicator);
    });
    $(document).on('dragstop', '.codeblock:not(.start,#run,#modeChange)', function(event, ui) {
        var newElement = $(event.target).clone();
        $('.drop').replaceWith(newElement);
        if ($(event.target).parent('.buttons').length === 0)
            $(event.target).remove();
    });
}
function createBindingsAndListeners() {
    var currentProgram = undefined;
    $('#modeChange').on('click', function(event) {
        var span = $(this).children('span').first();
        var currentViewportId = $('#main').children().filter(':not(.border):visible').attr('id');
        if (currentViewportId === 'simulation') {
            var sim = SimulatorFactory.get().getCurrentSimulator();
            if (typeof sim !== 'undefined')sim.simulator.abort();
//            Simulation.get().abort();
            if (span.html() === 'T') {
                setViewport('#program');
            }
            else {
                setViewport('#textualProgram');
            }
            $('#run').children().first().html('Run').parent().css('background-color', '#0f0');

            return;
        }
        if (span.html() === 'T') {
            var program = Program.fromGraphics($('#program'));
            $('#textualProgram').val(program.toText());
            $('#program').find('li:not(.start)').remove();
            setViewport('#textualProgram');
            span.html('G');
        } else {
            var program = Program.fromText($('#textualProgram'));
            debug('isValid', program.isValid());
            if (!program.isValid() && !confirm('program contains unknown code, and will be ruined by this transformation. Continue?')) {
                return;
            }
            program.toGraphics().forEach(function(element) {
                if (element !== undefined)
                    CodeblockManager.get().insert($('#program'), element);
            });
            setViewport('#program');
            $('#textualProgram').text('');
            span.html('T');
        }
    });
    debug('creating listener for run button');
    $('#run').on('click', function() {
        var runSpan = $(this).children().first();
        if (runSpan.html() === 'Abort') {
            var sim = SimulatorFactory.get().getCurrentSimulator();
            if (typeof sim !== 'undefined')sim.simulator.abort();
            $(this).css('background-color', '#0f0');
            return;
        }
        var currentViewportId = $('#main').children().filter(':not(.border):visible').attr('id');
        if (currentViewportId === 'program') {
            currentProgram = Program.fromGraphics($('#program'));
        } else if (currentViewportId === 'textualProgram') {
            currentProgram = Program.fromText($('#textualProgram'));
        }
        if (!currentProgram.isValid()){
            alert('You have an invalid program.');
            return;
        }
        setViewport('#simulation');
        var rerun = runSpan.html() === 'Rerun';
        SimulatorFactory.get().getSimulatorForCanvas($('#simulation')).run(runSpan, currentProgram.toRunnableProgram());
//        Simulation.get().run(runSpan, currentProgram.toRunnableProgram());
    });
    $(document).on('keydown', 'input.numbers', function(e) {
        var w = e.which;
        debug(w);
        if (e.shiftKey === true) {
            if (w === 9)
                return true;
            return false;
        }
        if (e.shiftKey || e.ctrlKey || e.metaKey)
            return false;
        if (w === 109 || w === 189) {
            var t = $(this);
            var hasMinus = t.val().charAt(0) === '-';
            if (hasMinus) {
                t.val(t.val().slice(1));
            } else {
                t.val('-' + t.val());
            }
            return false;
        }
        if (w >= 96 && w <= 109)
            return true;
        if (w > 57)
            return false;
        return true;
    });
    $(document).on('keydown', 'textarea', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 9) {
            e.preventDefault();

            var start = this.selectionStart;
            var end = this.selectionEnd;
            var that = $(this);
            var current = that.val();
            if (start === end) {
                //Normal tab input
                that.val(current.substring(0, start)
                        + '    '
                        + current.substring(end));
                this.selectionStart = this.selectionEnd = start + 4;
            } else {
                //Something is selected, and we want every line affected to receive a tab at its start.
                var lines = current.split('\n');
                var charactersSeen = 0;
                var affectedLines = 0;
                for (var i = 0; i < lines.length; i++) {
                    var lineStart = charactersSeen, lineEnd = charactersSeen + lines[i].length;
                    if (Math.max(0, Math.min(end, lineEnd) - Math.max(start, lineStart)) > 0) {
                        lines[i] = '    ' + lines[i];
                        charactersSeen -= 4;
                        affectedLines++;
                    }
                    charactersSeen += lines[i].length + 1;
                }
                that.val(lines.join('\n'));
                this.selectionStart = start + 4;
                this.selectionEnd = end + 4 * affectedLines;
            }
        }
    });
    $('#clearProgram').on('click', function() {
        var currentViewportId = $('#main').children().filter(':not(.border):visible').attr('id');
        if (currentViewportId === 'program') {
            $('#program').find('li:not(.start)').remove();
        } else if (currentViewportId === 'textualProgram') {
            $('#textualProgram').val('');
        }
    });
    $('#options').on('click', function() {
        console.debug('options click');
        toggleOptions();
    });
}
function sendTestEvent() {
    var event; // The custom event that will be created

    if (document.createEvent) {
        event = document.createEvent("HTMLEvents");
        event.initEvent("deviceready", true, true);
    } else {
        event = document.createEventObject();
        event.eventType = "deviceready";
    }

    event.eventName = "deviceready";

    if (document.createEvent) {
        document.dispatchEvent(event);
    } else {
        document.fireEvent("on" + event.eventType, event);
    }
}
function setViewport(selector) {
    debug('setting viewport: ' + selector);
    var currentViewport = $('#main').children().filter(':not(.border):visible');
    var newViewport = $('#main').children().filter(selector);
    currentViewport.hide();
    newViewport.show();
}
function toggleOptions() {
    console.debug('optionsToggle');
    Options.get().toggle();
}