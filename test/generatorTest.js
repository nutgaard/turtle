function generatorTests() {

    test("Generator test", function() {
        for (var testId = 0; testId < generatorTests.length; testId++) {
            var test = generatorTests[testId];
            deepEqual(createProgramSequence(test.program.slice(0)), test.expected, test.name);
        }
    });
    function createProgramSequence(program) {
        var p = new Program(program.filter(function(cmd) {
            return cmd.trim().length > 0;
        }).map(function(cmd) {
            return cmd.trim();
        }));
        var engine = new jsEngine();
        var js = engine.getJS(p);
        var gen = new jsEngineGenerator(js);
        gen.generate(30000);
        return gen.getProgram();
    }
    var generatorTests = [{
            program: ['FWD 10'],
            expected: ['FWD 10'],
            name: 'Single command generator'
        },
        {
            program: ['FWD 10', 'TL 90'],
            expected: ['FWD 10', 'TL 90'],
            name: 'Sequential commands'
        }, {
            program: ['FWD 100', 'BACK 50'],
            expected: ['FWD 100', 'BACK 50'],
            name: 'Sequential commands2'
        }, {
            program: ['PROC A', 'FWD 10', 'TL 90', 'END'],
            expected: [],
            name: 'Non-called procedure definition'
        }, {
            program: ['PROC A', 'FWD 10', 'TL 90', 'END', 'CALL A'],
            expected: ['FWD 10', 'TL 90'],
            name: 'Called procedure definition'
        }, {
            program: ['PROC A', 'FWD 10', 'TL 90', 'END', 'CALL A', 'CALL A'],
            expected: ['FWD 10', 'TL 90', 'FWD 10', 'TL 90'],
            name: 'Doubly-called procedure definition'
        }, {
            program: ['REP 1', 'FWD 10', 'TL 90', 'END'],
            expected: ['FWD 10', 'TL 90'],
            name: 'Single loop test. (REP 1)'
        }, {
            program: ['REP 2', 'FWD 10', 'TL 90', 'END'],
            expected: ['FWD 10', 'TL 90', 'FWD 10', 'TL 90'],
            name: 'Single loop test. (REP 2)'
        }, {
            program: ['REP 4', 'REP 4', 'FWD 10', 'TL 90', 'END', 'END'],
            expected: [
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90'
            ],
            name: 'Double loop test. (REP 4x4)'
        }, {
            program: ['REP 4', 'REP 4', 'REP 4', 'FWD 10', 'TL 90', 'END', 'END', 'END'],
            expected: [
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90'
            ],
            name: 'Trippel loop test. (REP 4x4x4)'
        }, {
            program: ['SET A 20', 'FWD A'],
            expected: ['FWD 20'],
            name: 'Single variable setting and getting'
        }, {
            program: ['SET A 20', 'FWD A', 'SET A 10', 'FWD A'],
            expected: ['FWD 20', 'FWD 10'],
            name: 'Single variable setting and getting'
        }, {
            program: [
                'PROC A', 'CALL B', 'FWD 10', 'PROC B', 'FWD 20', 'END', 'CALL B', 'END', 'PROC B', 'FWD 30', 'END', 'CALL B', 'CALL A'
            ],
            expected: [
                'FWD 30', 'FWD 20', 'FWD 10', 'FWD 20'
            ],
            name: 'Correct procedure scoping, override parent'
        }, {
            program: [
                'PROC A', 'FWD 10', 'CALL B', 'END', 'PROC B', 'FWD 20', 'END', 'CALL B', 'CALL A'
            ],
            expected: [
                'FWD 20', 'FWD 10', 'FWD 20'
            ],
            name: 'Correct procedure scoping, inherit from parent'
        }, {
            program: [
                'SET B 10', 'PROC A', 'SET B 20', 'FWD B', 'END', 'CALL A', 'FWD B'
            ],
            expected: [
                'FWD 20', 'FWD 10'
            ],
            name: 'Correct variable scoping between procedures, override but dont alter parent'
        }, {
            program: [
                'SET A 10', 'REP 1', 'SET A 20', 'FWD A', 'END', 'FWD A'
            ],
            expected: [
                'FWD 20', 'FWD 20'
            ],
            name: 'Correct variable scoping between repeats, override but dont alter parent'
        }, {
            program: [
                'SET B 10', 'PROC A', 'FWD B', 'END', 'CALL A', 'FWD B'
            ],
            expected: [
                'FWD 10', 'FWD 10'
            ],
            name: 'Correct variable scoping between procedures, inherit from parent'
        }, {
            program: [
                'SET A 10', 'REP 1', 'FWD A', 'END', 'FWD A'
            ],
            expected: [
                'FWD 10', 'FWD 10'
            ],
            name: 'Correct variable scoping between repeats, inherit from parent'
        }, {
            program: [
                'PROC A',
                '   FWD B',
                '   TL 90',
                'END',
                'SET B 50',
                'REP 5',
                '   REP 4',
                '       CALL A',
                '   END',
                '   SET B B-10',
                'END'
            ],
            expected: [
                'FWD 50', 'TL 90', 'FWD 50', 'TL 90', 'FWD 50', 'TL 90', 'FWD 50', 'TL 90',
                'FWD 40', 'TL 90', 'FWD 40', 'TL 90', 'FWD 40', 'TL 90', 'FWD 40', 'TL 90',
                'FWD 30', 'TL 90', 'FWD 30', 'TL 90', 'FWD 30', 'TL 90', 'FWD 30', 'TL 90',
                'FWD 20', 'TL 90', 'FWD 20', 'TL 90', 'FWD 20', 'TL 90', 'FWD 20', 'TL 90',
                'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90', 'FWD 10', 'TL 90',
            ],
            name: 'Variables, Procedures and nested Repeats'
        }
    ];
}